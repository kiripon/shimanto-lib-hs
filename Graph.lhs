>module Graph where
>import Control.Monad
>import Control.Monad.State
>import Control.Applicative
>import Data.Array
>import Data.Array.ST
>import Data.Array.IO
>import qualified Data.Set as S
>import Data.Traversable
>type Dist = Int
>type Node = Int
>type Graph = Array Node [(Node,Dist)]

2.1 ダイクストラ
==========

>dijkstra :: Graph -> Node -> Array Node Dist
>dijkstra g s = runSTArray $ do
>  a <- newArray bound maxBound
>  evalStateT (loop a) (S.singleton (0,s))
>  return a
>  where
>    bound = bounds g
>    loop a = do
>      isEmpty <- S.null <$> get
>      unless isEmpty $ do
>        (d,n) <- state S.deleteFindMin
>        d' <- lift $ readArray a n
>        when (d < d') $ do
>          lift $ writeArray a n d
>          let nexts = g ! n
>          void $ for nexts $ \(ix,w) -> do
>            d'' <- lift $ readArray a ix
>            when (d + w < d'')
>              $ modify $ S.insert (d+w,ix)
>        loop a

>getInput :: IO (Graph,Node)
>getInput = do
>  v:e:r:_ <- map read . words <$> getLine :: IO [Int]
>  a <- newArray (0,v-1) [] :: IO (IOArray Node [(Node,Dist)])
>  replicateM_ e $ do
>    s:t:d:_ <- map read . words <$> getLine :: IO [Int]
>    es <- readArray a s
>    writeArray a s ((t,d):es)
>  a' <- freeze a
>  return (a',r)
