> {-# LANGUAGE BangPatterns ,DeriveFunctor#-}

> module Geometry where
> import Control.Applicative
> ε :: Double
> ε = 10e-10
> {-# INLINE ε #-}

> data V2 a = V2 {
>   x :: !a,
>   y :: !a
>   } deriving (Functor,Show)

> type Point = V2 Double

> instance Num a => Num (V2 a) where
>   p + q = V2 (x p + x q) (y p + y q)
>   {-# SPECIALIZE INLINE (+) :: V2 Double -> V2 Double -> V2 Double #-}
>   negate p = fmap negate p
>   {-# SPECIALIZE INLINE negate :: V2 Double -> V2 Double #-}
>   p * q = V2 (x p * x q) (y p * y q)
>   {-# SPECIALIZE INLINE (*) :: V2 Double -> V2 Double -> V2 Double #-}
>   abs p = fmap abs p
>   {-# SPECIALIZE INLINE abs :: V2 Double -> V2 Double #-}
>   signum p = fmap signum p
>   {-# SPECIALIZE INLINE signum :: V2 Double -> V2 Double #-}
>   fromInteger n = let n' = fromInteger n in V2 n' n'
>   {-# SPECIALIZE INLINE fromInteger :: Integer -> V2 Double #-}

* 線分

> data LineSegment a = LS !(V2 a) !(V2 a)

* 外積

> det :: Num a => V2 a -> V2 a -> a
> det p1 p2 = x p1 * y p2 - x p2 * y p1
> {-# SPECIALIZE INLINE det :: V2 Double -> V2 Double -> Double #-}

* 内積

> dot :: Num a => V2 a -> V2 a -> a
> dot p1 p2 = x p1 * x p2 + y p1 * y p2
> {-# SPECIALIZE INLINE det :: V2 Double -> V2 Double -> Double #-}
> {-# INLINE dot #-}

* ベクトルのノルム

> norm :: Floating s => V2 s -> s
> norm p = sqrt $ dot p p
> {-# SPECIALIZE INLINE norm :: V2 Double -> Double #-}

* 法線ベクトル

> normal :: Floating a => V2 a -> V2 a
> normal p = V2 y' x'
>   where
>     n = norm p
>     x' = negate (x p / n)
>     y' = y p / n
> {-# SPECIALIZE INLINE normal :: V2 Double -> V2 Double #-}    

* ccw関数

> ccw :: Point -> Point -> Point -> Int
> ccw !a !b !c
>   | det p q < ε                =  1 -- counter clockwise
>   | det p q > negate ε         = -1 -- clockwise
>   | dot p q < negate ε         =  2 -- on line like "c--a--b"
>   | norm p < norm q - negate ε = -2 -- on line like "a--b--c"
>   | otherwise                  =  0 -- a--c--b or (b == c)
>   where
>     p = b - a
>     q = c - a
> {-# INLINE ccw #-}

* 線分の交差判定

> isIntersect :: LineSegment Double -> LineSegment Double -> Bool
> isIntersect !(LS p1 p2) !(LS q1 q2) = ccw1 < 0 && ccw2 < 0
>   where
>     ccw1 = ccw q1 q2 p1 * ccw q1 q2 p2 
>     ccw2 = ccw p1 p2 q1 * ccw p1 p2 q2 
> {-# INLINE isIntersect #-}

* 2直線の交点を求める

> intersection :: LineSegment Double -> LineSegment Double -> Point
> intersection (LS a1 a2) (LS b1 b2)
>   | abs a' < ε && abs b' < ε = a1
>   | otherwise = a1 + ((*) (a'/b')  <$> a)
>   where
>     a = a2 - a1
>     b = b2 - b1
>     a' = det b (b1 - a1)
>     b' = det b a

> type Polygon = [Point]

> isContainedIn :: [Point] -> Point -> Bool
> isContainedIn poly q = odd $ aux poly (0 :: Int)
>   where
>     p0 = head poly
>     halfLine = LS q (V2 10e20 (y q))
>     aux (p1:p2:ps) n = aux (p2:ps) (if isIntersect (LS p1 p2) halfLine then n+1 else n)
>     aux (p1:[]) n = if isIntersect (LS p1 p0) halfLine then n+1 else n

* 点と直線の距離. 平行四辺形の面積/底辺の長さ で求めている

> distanceLinePoint :: Point -> LineSegment Double -> Double
> distanceLinePoint p (LS p1 p2) = abs $ det (p2 - p1) (p - p1) / norm (p2 - p1)
