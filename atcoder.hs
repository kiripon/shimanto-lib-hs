{-# LANGUAGE BangPatterns #-}
module Main (main) where

import qualified Data.ByteString as S
import qualified Data.ByteString.Char8 as S8
import           Data.Array.IO as AIO
import           Data.Array
import Data.IORef
import Data.Array.Unsafe
import Control.Applicative
import Control.Monad (replicateM_,forM_)
import Data.Maybe (fromJust)
import Data.List (unfoldr)

readInt :: S8.ByteString -> Int
readInt = fst.fromJust.S8.readInt

readInts :: S8.ByteString -> [Int]
readInts = map readInt .S8.words

modifyArray :: (MArray a e m, Ix i) => a i e -> i -> (e -> e) -> m ()
modifyArray a i f = do
  x <- readArray a i
  writeArray a i (f x)

chunksOf :: Int -> [a] -> [[a]]
chunksOf n = unfoldr $ (\x -> if null . fst $ x then Nothing else Just x) . splitAt n


main = undefined
