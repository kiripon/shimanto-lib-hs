1 数論
=======

インポートするモジュール

> {-# LANGUAGE BangPatterns #-}
> module NumTheory where
> import Control.Applicative
> import Data.List (foldl')
> import Data.Array.ST
> import Data.Array
> import Control.Monad.ST
> import Control.Monad

1.1 エラトステネスのふるい
======================

$n$以下の自然数について、素数かどうかのブール値を持ったArrayを返す。

* コード

> sieve :: Int -> Array Int Bool
> sieve n = runSTArray $ do
>   isPrime <- newArray (1,n) True
>   writeArray isPrime 1 False
>   forM_ [2 .. n] $ \i -> do
>     ip <- readArray isPrime i
>     when ip $ do
>       forM_ [i*i,i*i+i .. n] $ \x -> do
>         writeArray isPrime x False
>   return isPrime


> primes :: Int -> [Int]
> primes n = [x | x <- [2..n], isPrime ! x]
>   where isPrime = sieve n


1.2 ガウスの消去法
================

ガウスの消去法で$ A x = b$を解く。

*TODO*:整数の計算の場合はまだ考えいない.invert と modulo を定義する必要がある。

1.3 Joseph's Number
===================

コード
-----

> josephus :: Int -> Int -> Int -> Int
> josephus n m 1 = (m - 1) `mod` n
> josephus n m k = (josephus (n-1) m (k-1) + m) `mod` n

1.4 黄金分割法
===================

1.5 最大公約数,最小公倍数
======================

HaskellではPreludeにgcdとlcmが定義されているので自分で定義する必要はない。
extgcd a b は、

$$
a * x + b * y = gcd a b 
$$

となる(gcd a b,a,b)を返す


> extgcd :: Int -> Int -> (Int, Int, Int)
> extgcd a b
>   | a > b  = let (g,p,q) = extgcd a b
>             in (g,q,p)
>   | a == 0 = (b,0,1)
>   | otherwise = let (g,p,q) = extgcd (b`mod`a) a
>                     z = q - p*(b `div` a)
>                 in (g,z,p)


g = b * p + a * z
  = (b `mod` a) * p + [b - b `mod` a] * p  + a * z
  = (b `mod` a) * p + a * (b`div`a) * p + a * z
  = (b `mod` a) * p + a * (z + p*(b `div` a))
  = (b `mod` a) * p + a * q


素因数分解
=========================

T(primeFactor)(n) = O(√n)

> primeFactor :: Int -> [Int]
> primeFactor = primeFactor' 2
>   where
>     divloop !n !d = if n `mod` d == 0 then divloop (n `div` d) d else n
>     primeFactor' !ix !n
>       | n `mod` ix == 0 = ix : primeFactor' (ix+1) (n `divloop` ix)
>       | ix * ix <= n    = primeFactor' (ix+1) n
>       | n == 1          = []
>       | otherwise       = [n]

T(primeFactor_naive)(n) = O(n)

> primeFactor_naive :: Int -> [Int]
> primeFactor_naive = primeFactor' 2
>   where
>     divloop !n !d = if n `mod` d == 0
>                     then divloop (n `div` d) d
>                     else n
>     primeFactor' !ix !n
>       | n `mod` ix == 0 = ix : primeFactor' (ix+1) (n `divloop` ix)
>       | n == 1          = []
>       | otherwise = primeFactor' (ix+1) n


1.6 オイラーのφ関数
==================

φ(x) = #{1~x までの整数のうちで gcd(x,1) == 1 なものの数}
をオイラーのφ関数とよぶ。
p ⊥ q のとき、 φ(pq) = φ(p)φ(q)が成り立つ。
p が素数のとき φ(p^a) = p^a - {pの倍数} = p^a - p^(a - 1)

> totient :: Int -> Int
> totient n = foldl' f n $ primeFactor n
>   where
>     f acc n = acc - acc `div` n

