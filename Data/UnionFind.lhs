>module Data.UnionFind where

>import Data.Array.Unboxed
>import Data.Array.IO

>type UnionFind = IOUArray Int Int
>type Node = Int

>root :: UnionFind -> Int -> IO Node
>root uf i = do
>   p <- readArray uf i
>   if p == i
>     then return i
>     else do
>     r <- root uf p
>     writeArray uf i r
>     return r


>findSet :: Int -> Int -> UnionFind -> IO Bool
>findSet i j s = do
>   x <- root s i 
>   y <- root s j
>   return (x == y)

>unionSet :: Int -> Int -> UnionFind -> IO ()
>unionSet i j u = do
>   x <- root u i
>   y <- root u j
>   writeArray u x y
