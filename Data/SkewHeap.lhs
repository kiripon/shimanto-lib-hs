> module Data.SkewHeap
>        (isEmpty
>        ,minElem
>        ,deleteElem
>        ,insert
>        )where
> import Prelude(Bool(..),Ord,(<=),otherwise)
> data Tree a = Null | Fork a (Tree a) (Tree a)

> isEmpty :: Tree t -> Bool
> isEmpty Null = True
> isEmpty (Fork _ _ _) = False

> minElem :: Tree t -> t
> minElem (Fork x _a _b) = x

> insert :: Ord a => a -> Tree a -> Tree a
> insert x a = merge (Fork x Null Null) a

> deleteElem :: Ord a => Tree a -> Tree a
> deleteElem (Fork _x a b) = merge a b

> merge :: Ord a => Tree a -> Tree a -> Tree a
> merge a Null = a
> merge Null b = b
> merge a b
>   | minElem a <= minElem b = join a b
>   | otherwise              = join b a

> join :: Ord a => Tree a -> Tree a -> Tree a
> join (Fork x a b) c = Fork x b (merge a c)
