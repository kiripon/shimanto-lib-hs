>{-# LANGUAGE TypeOperators #-}

実装中。まだ書き終わってない。

>module Data.PSQueue where

k,pはOrdのインスタンス

>data PSQ k p = Void 
>             | Win (k,p) (LTree k p) k


* *Void*         .. 空

`Winner b t m`{.haskell} は以下を表す

* `b` .. 勝者
* `t` .. assoc loser tree
* `m` .. max key

>data LTree k p = Start
>               | Lose (k,p) (LTree k p) k (LTree k p)

*Loser b $t_l$ k $t_r$* は以下を表す

* b .. 負けたやつ
* k .. split key
* $t_l$ .. left-subtree
* $t_r$ .. right-subtree

>maxKey :: PSQ k p -> k
>maxKey (Win _ _ m) = m
>maxKey _           = error "maxKey:empty queue"

Priority search pennantsは semi-heap と search tree の特徴を組み合わせている。
以下の条件を満たす。

__Semi-heap contidionts__

* すべてのpennantのpriorityはwinner以上でなければならない
* loser treeのすべてのノードについて,loserの持つ値(binding)はそのloserから始まるsubtreeのbinding以下でなければならない。\
  keyがsplit key以下であるとき「left subtree から始まる」という。\
  そうでない時は「right subtreeから始まる」とよぶ。

__Search-tree codition__

* すべての node について、\
  left-subtree の key は split key 以下でなければならない.\
  right-subtree の key は split key より大きくなければならない。

__Key condition__

* max-keyとsplit-keyはbindingのkeyとして現れなければならない。

__Finite map condition__

* Pennantは同じkeyが同時に２つのbindingを含んではならない

Constructor
===========

空の木を作る

>empty :: (Ord k,Ord p) => PSQ k p
>empty = Void

1要素だけの木をつくる

>singleton :: (Ord k,Ord p) => (k, p) -> PSQ k p
>singleton b = Win b Start (key b)

`play`は2つの木の和を作る。
はじめの木のすべてのkeyは2つ目の木のkeyより小さいことが必要.


>play :: (Ord k, Ord p) => PSQ k p -> PSQ k p -> PSQ k p
>Void      `play` t    = t
>t         `play` Void = t
>Win b t m `play` Win b' t' m'
>  | prio b <= prio b'    = Win b  (Lose b' t m t') m'
>  | otherwise            = Win b' (Lose b  t m t') m'

`fromOrdList`はソート済みのリストからPSQueueを構築する.

>fromOrdList :: (Ord k,Ord p) => [(k ,p)] -> PSQ k p
>fromOrdList = foldm play empty . map singleton

fromOrdList [(A,4),(D,2),(E,1),(J,6),(L,3),(N,7),(P,5),(V,8)]
からは
```
(({A 7→ 4} `play` {D 7→ 2})
  `play`
 ({E 7→ 1} `play` {J 7→ 6}))
`play`
(({L 7→ 3} `play` {N 7→ 7})
  `play`
 ({P 7→ 5} `play` {V 7→ 8}))
```

5.2 Destructors
===============

>data PSQView k p = Empty | Min (k +-> p) (PSQ k p)

>psqView :: (Ord p, Ord k) => PSQ k p -> PSQView k p
>psqView Void        = Empty
>psqView (Win b t m) = Min b (secondBest t m)

>secondBest :: (Ord p, Ord k) => LTree k p -> k -> PSQ k p
>secondBest Start _    = Void
>secondBest (Lose b t k u) m
>  | key b <= k        = Win b t k `play` secondBest u m
>  | otherwise         = secondBest t k `play` Win b u m

>deleteMin :: (Ord p, Ord k) => PSQ k p -> PSQ k p
>deleteMin Void           = Void
>deleteMin (Win _ Start _) = Void
>deleteMin (Win b (Lose b' t k u) m)
>  | key b' <= k = Win b' t k `play` deleteMin (Win b u m)
>  | otherwise   = deleteMin (Win b t k) `play` Win b' u m

* `deleteMin (Winner b t m) = secondBest t m`{.haskell}が成り立つ



key操作系
========

>type k +-> p = (k,p)

>key :: (k +-> p) -> k
>key (k, _) = k

>prio :: (k +-> p)-> p
>prio (_, p) = p


おまけ
=====

>foldm :: (a -> a -> a) -> a -> [a] -> a
>foldm f e xs
>  | null xs   = e
>  | otherwise = fst (go (length xs) xs)
>  where
>    go 1 (a:as) = (a,as)
>    go n as     = (a1 `f` a2, as2)
>      where
>        m = n `div` 2
>        (a1,as1) = go (n - m) as
>        (a2,as2) = go m as1
