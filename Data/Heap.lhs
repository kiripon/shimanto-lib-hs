> {-# LANGUAGE BangPatterns #-}
> module Data.Heap where
> data Heap a = Null | Fork a (Heap a) (Heap a) deriving Show

> foldHeap :: b -> (a -> b -> b -> b) -> Heap a -> b
> foldHeap c f h = case h of
>   Null -> c
>   Fork x l r -> f x (foldHeap c f l) (foldHeap c f r)

> flatten :: (Ord a) => Heap a -> [a]
> flatten Null = []
> flatten (Fork x lt rt) = x:merge (flatten lt) (flatten rt)

> merge :: Ord a => [a] -> [a] -> [a]
> merge [] ys = ys
> merge xs [] = xs
> merge xs'@(x:xs) ys'@(y:ys) = if x < y
>                             then x : merge xs ys'
>                             else y : merge xs' ys

> ordered :: Ord b => [b] -> Bool
> ordered xs = and $ zipWith (<) xs (tail xs)

> heapOrdered :: Ord a => Heap a -> Bool
> heapOrdered = ordered . flatten



> heapify :: Ord a => Heap a -> Heap a
> heapify = foldHeap Null sift

> sift :: Ord t => t -> Heap t -> Heap t -> Heap t
> sift x Null Null = Fork x Null Null
> sift x (Fork y a b) Null = if x <= y
>                            then Fork x (Fork y a b) Null
>                            else Fork y (sift x a b) Null
> sift x Null (Fork y a b) = if x < y
>                            then Fork x Null (Fork y a b)
>                            else Fork y Null (sift x a b)
> sift x (Fork y a b) (Fork z c d)
>   | x <= (y `min` z) = Fork x (Fork y a b) (Fork z c d)
>   | y <= (x `min` z) = Fork y (sift x a b) (Fork z c d)
>   | z <= (x `min` z) = Fork z (Fork y a b) (sift x c d)

> levels :: [a] -> [[a]]
> levels = levelsWith 1
>   where
>     levelsWith !n !xs
>       = let (a,b) = splitAt n xs
>         in case a of
>             [] -> []
>             _  -> a : levelsWith (2*n) b

> mkHtrees :: (Ord a) => [[a]] -> [Heap a]
> mkHtrees = foldr addLayer [Null]


> addLayer :: [a] -> [Heap a] -> [Heap a]
> addLayer as bs = aux as $ bs ++ repeat Null
>   where
>     aux [] _ = []
>     aux (x:xs) (l:r:ts) = Fork x l r : addLayer xs ts

> mkHtree :: Ord a => [a] -> Heap a
> mkHtree = head . mkHtrees . levels

> makeHeap :: Ord a => [a] -> Heap a
> makeHeap = heapify . mkHtree
