module Main where
import NumTheory
import Data.List(sort)
import Data.Ratio

main :: IO ()
main = do
  let l = [2..10^(7::Int)] :: [Int]
  print $ minimum . map f . filter p . zip l . map totient $ l
  where
    p (n,phi) = (sort.show) n == (sort.show) phi
    f (n,phi) = (n % phi,n)
