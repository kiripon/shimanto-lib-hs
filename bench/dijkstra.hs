{-# LANGUAGE BangPatterns #-}

module Main (main)
       where
import Control.Applicative
import Control.Monad
import Data.Array
import Data.Array.IO
import Data.Array.Unsafe
import qualified Data.ByteString.Char8 as BS
import Data.Maybe
import Graph(dijkstra,Graph,Node,Dist)

readInt' :: BS.ByteString -> Int
readInt' = fst.fromJust.BS.readInt

getInput :: IO Graph
getInput = do
  n <- readInt' <$> BS.getLine
  g <- newArray (0,n-1) [] :: IO (IOArray Node [(Node,Dist)])
  replicateM_ n $ do
    u:_:vs <- map readInt' . BS.words <$> BS.getLine :: IO [Int]
    let loop xs acc = case xs of
          [] -> acc
          [_] -> undefined
          a:b:ys -> loop ys ((a,b):acc)
    writeArray g u $ loop vs []
  unsafeFreeze g

main :: IO ()
main = do
  g <- getInput
  let ans = dijkstra g 0
  forM_ (assocs ans) $ \ (i,e) -> do
    putStrLn $ show i ++ ' ':if e == maxBound
                             then "INF"
                             else show e
  return ()
